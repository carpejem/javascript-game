// alert("w0rk3ng??");



let pScore = 0;

let cScore = 0;

let playerBtn = document.querySelector(".intro button");

let introScreen = document.querySelector(".intro");

let match = document.querySelector(".match");

let options = document.querySelectorAll(".options button");

let playerHand = document.querySelector(".player-hand");

let computerHand = document.querySelector(".computer-hand");

let hands = document.querySelectorAll(".hands img");

let playerScore = document.querySelector(".player-score p");

let computerScore = document.querySelector(".computer-score p");

let winner = document.querySelector(".winner");

let computerOptions = ["ROCK", "PAPER", "SCISSORS"];




function game() {
	// tawagin mo ang mga function kundi hindi gagana
	startGame();
	playMatch();
	
};

// para makapaglaro ka
game();





	function startGame() {
		playerBtn.addEventListener("click", function(){
			introScreen.classList.add("fadeOut");
			match.classList.add("fadeIn");
		});
	};



	function playMatch() {

		hands.forEach( function(hand){
			hand.addEventListener("animationend", function(){
				this.style.animation = "";
			});
		});


		// computer options
		options.forEach( function(option){
			option.addEventListener("click", function(){
				// computer choice
				computerNumber = Math.floor(Math.random()*3);
				computerChoice = computerOptions[computerNumber];

				setTimeout(() =>{
					// here is where we call compare hands
				compareHands(this.textContent, computerChoice);

				// update images
				playerHand.src = `assets/images/${this.textContent}.svg`;
				computerHand.src = `assets/images/${computerChoice}.svg`;
				}, 2000);
				

				// animation
				playerHand.style.animation = "shakePlayer 2s ease";
				computerHand.style.animation = "shakeComputer 2s ease";
			});
		});
	};




	function updateScore() {
		playerScore.textContent = pScore;
		computerScore.textContent = cScore;
	};




	function compareHands(playerChoice, computerChoice) {
		// update text
		if (playerChoice === computerChoice) {
			winner.textContent = "DRAW!";
			return;
		}
		// check for rock
		if (playerChoice === "ROCK"){
			if (computerChoice === "SCISSORS") {
				winner.textContent = "YOU WINNNN!!";
				pScore++;
				updateScore();
				return;
			} else {
				winner.textContent = "YOU LOST..";
				cScore++;
				updateScore();
				return;
			};
		};
		//check for paper
		if (playerChoice === "PAPER"){
			if (computerChoice === "SCISSORS") {
				winner.textContent = "YOU LOST..";
				cScore++;
				updateScore();
				return;
			} else {
				winner.textContent = "YOU WINNNN!!";
				pScore++;
				updateScore();
				return;
			};
		};
		// check for scissors
		if (playerChoice === "SCISSORS"){
			if (computerChoice === "ROCK") {
				winner.textContent = "YOU LOST..";
				cScore++;
				updateScore();
				return;
			} else {
				winner.textContent = "YOU WINNNN!!";
				pScore++;
				updateScore();
				return;
			};
		};
	};
























